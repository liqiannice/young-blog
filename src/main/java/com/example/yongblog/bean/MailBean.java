package com.example.yongblog.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by LiQian_Nice on 2018/7/6
 */
@Data
public class MailBean implements Serializable {

    private String from = "51103942@qq.com";
    private String fromName;
    private String[] toEmails;

    private String subject;

    private Map data ;          //邮件数据
    private String template;    //邮件模板
}
