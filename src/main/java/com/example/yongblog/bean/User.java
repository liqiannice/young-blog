package com.example.yongblog.bean;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by LiQian_Nice on 2018/7/1
 */
@Data
@Entity
@Table
public class User {

    @Id
    @Column
    @GeneratedValue
    private Integer id;

    private String email;

    private String password;

    private String nickname;

    private String avatar;

    private String bindType;

    private String uniqueValue;

    private String createTime ;

    private Boolean status ;

}
