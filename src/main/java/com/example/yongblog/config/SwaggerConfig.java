package com.example.yongblog.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by LiQian_Nice on 2018/2/21
 */
/*重要！如果你的项目引入junit测试，此处需要使用@WebAppConfiguration，如果没有使用junit使用@Configuration(很多的博客都没有注明这个问题，为此我花了非常多的时间解决问题)*/
@Configuration
@EnableSwagger2//重要！
@EnableWebMvc
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {//api文档实例

        return new Docket(DocumentationType.SWAGGER_2)//文档类型：DocumentationType.SWAGGER_2
                .apiInfo(apiInfo())//api信息
                .useDefaultResponseMessages(false)
                .forCodeGeneration(true)
                .select()//构建api选择器
                .apis(RequestHandlerSelectors.basePackage("com.example.yongblog.controller"))//api选择器选择api的包
                //.apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())//api选择器选择包路径下任何api显示在文档中
                .build();//创建文档
    }

    private ApiInfo apiInfo() {//接口的相关信息
        return new ApiInfoBuilder()
                .title("faker")
                .description("Base By SSM")
                .termsOfServiceUrl("https://github.com/isliqian/faker")
                .contact(new Contact("李前Nice","https://github.com/isliqian","51103942@qq.com"))
                .version("1.0")
                .build();
    }
}
