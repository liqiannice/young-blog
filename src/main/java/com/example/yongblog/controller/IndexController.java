package com.example.yongblog.controller;




import com.example.yongblog.service.IGithubService;
import com.google.zxing.WriterException;

import com.example.yongblog.bean.User;

import com.example.yongblog.service.IMailService;
import com.example.yongblog.service.IUserService;

import com.example.yongblog.service.impl.GithubServiceImpl;
import com.example.yongblog.utils.QiNiuUtil;
import com.example.yongblog.zxing.QRCodeFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Created by LiQian_Nice on 2018/6/29
 */
@Controller
@Api(value = "/courses", tags = "接口",description = "学生课程接口")
@Slf4j
public class IndexController {



    @Resource
    private IUserService userService;

    @Resource
    private IGithubService githubService;

    @Resource
    private IMailService mailService;

    @Resource
    private MessageSource messageSource;

    private QiNiuUtil qiNiuUtil;

    @GetMapping(value = "/")
    @ApiOperation(value = "主界面",notes = "主界面",httpMethod = "GET")
    public ModelAndView dashboard(){
        ModelAndView mv =new ModelAndView();
        Locale locale = LocaleContextHolder.getLocale();
        String language = messageSource.getMessage("language", null, locale);//国际化
        String languageHref = messageSource.getMessage("language.href", null, locale);

        mv.addObject("language",language);
        mv.addObject("language.href",languageHref);
        mv.setViewName("/front/dashboard");
        return mv;
    }

    @GetMapping(value = "/profile")
    @ApiOperation(value = "个人界面",notes = "个人界面",httpMethod = "GET")
    public ModelAndView profile(){
        ModelAndView mv=new ModelAndView();
        setLang(mv);
        mv.setViewName("/front/profile");
        return mv;
    }
    @GetMapping(value = "/zxing")
    @ResponseBody
    @ApiOperation(value = "zxing",notes = "zxing",httpMethod = "GET")
    private void index(HttpServletResponse resp)throws IOException {
        String content="大家好，我是李庆文，很高兴认识大家";
        String logUri="https://upload.jianshu.io/users/upload_avatars/1599743/e716f8d47965?imageMogr2/auto-orient/strip|imageView2/1/w/96/h/96";
        int[]  size=new int[]{430,430};
        String format = "jpg";

        try {
            new QRCodeFactory().CreatQrImage(resp,content, format,  logUri,size);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (WriterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }
    @GetMapping(value = "/email")
    @ResponseBody
    @ApiOperation(value = "email",notes = "email",httpMethod = "GET")
    private String email(HttpServletResponse resp)throws IOException {
        try {
            System.out.println(1);
            mailService.sendEmail("");
        } catch (MessagingException e) {

            e.printStackTrace();
            return "false";
        }

        return "ok";


    }

    @GetMapping("/signin")
    @ApiOperation(value = "管理员登陆界面",notes = "管理员登陆",httpMethod = "GET")
    private ModelAndView signin(){
        ModelAndView mv =new ModelAndView();
        Locale locale = LocaleContextHolder.getLocale();
        String projectName = messageSource.getMessage("project.name", null, locale);//国际化
        String loginTitle = messageSource.getMessage("title", null, locale);
        String loginContent = messageSource.getMessage("login.content",null,locale);
        String loginEmail = messageSource.getMessage("login.email",null,locale);
        String loginPwd=messageSource.getMessage("login.pwd",null,locale);
        String loginRemember=messageSource.getMessage("login.remember",null,locale);
        String qrCode=messageSource.getMessage("login.qrCode",null,locale);
        String phone=messageSource.getMessage("login.phone",null,locale);
        String noAccount=messageSource.getMessage("login.noAccount",null,locale);
        String register=messageSource.getMessage("login.register",null,locale);

        String loginBtn=messageSource.getMessage("login.loginBtn",null,locale);

        mv.addObject("project.name",projectName);
        mv.addObject("title",loginTitle);
        mv.addObject("login.content",loginContent);
        mv.addObject("login.pwd",loginPwd);
        mv.addObject("login.email",loginEmail);
        mv.addObject("login.remember",loginRemember);
        mv.addObject("login.loginBtn",loginBtn);
        mv.addObject("login.qrCode",qrCode);
        mv.addObject("login.phone",phone);
        mv.addObject("login.noAccount",noAccount);
        mv.addObject("login.register",register);

        mv.setViewName("/front/signin");
        return mv;
    }

    @GetMapping("/github/login")
    public String getAhthcode(){
        String path = "https://github.com/login/oauth/authorize?client_id="+ GithubServiceImpl.GITHUB_CLIENT_ID+"&scope=user:email";
        return "redirect:"+path;
    }

    @GetMapping("/{type}/login/callback")
    public String doGithubLogin(@PathVariable(value = "type") String type,String code) throws IOException {
        User user=githubService.login(type,code);
        log.info(user.toString());
        if (user!=null){
            return "redirect:"+"http://localhost:8080/youngblog/";
        }else {
            return "/front/signin";
        }


    }

    public void setLang(ModelAndView mv){

        Locale locale = LocaleContextHolder.getLocale();
        String language = messageSource.getMessage("language", null, locale);//国际化
        String languageHref = messageSource.getMessage("language.href", null, locale);

        mv.addObject("language",language);
        mv.addObject("language.href",languageHref);

    }


}
