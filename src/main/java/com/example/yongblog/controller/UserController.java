package com.example.yongblog.controller;

import com.example.yongblog.bean.User;
import com.example.yongblog.service.IUserService;
import com.example.yongblog.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.sql.SQLException;

/**
 * Created by LiQian_Nice on 2018/7/20
 */
@Controller
@Api(value = "/user", tags = "用户Api",description = "用户Api")
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Resource
    private IUserService userService;

    @PostMapping("/signup")
    @ResponseBody
    @ApiOperation(value = "用户注册",notes = "用户注册",httpMethod = "POST")
    private Object registUser(User user) throws IllegalAccessException, SQLException, InvocationTargetException {
        log.info("邮箱为"+user.getEmail()+"的用户进行注册...");
        BigInteger count = userService.findByEmail(user.getEmail());

        if (count.intValue() > 1){
            log.info("邮箱为"+user.getEmail()+"的用户已经注册...");
        }else{
            userService.save(user);
            log.info("注册成功");
        }
        return ResultUtil.success();
    }


    @PostMapping("/signin")
    @ResponseBody
    @ApiOperation(value = "用户登陆",notes = "用户登陆",httpMethod = "POST")
    private Object loginUser(User user){
        ModelAndView mv =new ModelAndView();
        log.info("邮箱为"+user.getEmail()+"的用户进行登陆");
        User user1=userService.findByEmailAndPwd(user.getEmail(),user.getPassword());
        if(user1==null){
            return ResultUtil.error("用户不存在，请去注册一个账号");
        }
        if (user1.getStatus()==false){
            return ResultUtil.error("该用户还未激活,请去邮箱激活账号！");
        }
        return ResultUtil.success(user1);
    }
}
