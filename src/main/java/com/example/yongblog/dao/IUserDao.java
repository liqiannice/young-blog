package com.example.yongblog.dao;

import com.example.yongblog.bean.User;


import java.math.BigInteger;
import java.util.List;

/**
 * Created by LiQian_Nice on 2018/7/1
 */

public interface IUserDao {


    List<User> findAll();

    User findByUniqueValue(String uniqueValue);


    void save(User user);

    BigInteger findByEmail(String email);

    User findbyEmailAndPwd(String email, String password);
}
