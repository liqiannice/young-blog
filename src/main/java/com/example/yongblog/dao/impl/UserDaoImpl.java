package com.example.yongblog.dao.impl;

import com.example.yongblog.bean.User;
import com.example.yongblog.dao.IUserDao;
import com.example.yongblog.utils.TimeUtil;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by LiQian_Nice on 2018/7/20
 */
@Repository
@Transactional
public class UserDaoImpl implements IUserDao {

    @Resource
    private SessionFactory sessionFactory;
    public Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public List<User> findAll() {
        Query query = this.getSession().createQuery("from User ");
        List<User> list = query.list();
        return list;
    }
    @Override
    public User findByUniqueValue(String uniqueValue) {
        User user=(User) this.getSession()
                        .createQuery("from User where uniqueValue = ?")
                        .setParameter(0,uniqueValue)
                        .uniqueResult();
        
        return user;
    }


    @Override
    public void save(User user) {
        this.getSession().save(user);
    }

    @Override
    public BigInteger findByEmail(String email) {
        String sql="select count(id) from user u where u.email = ?";
        BigInteger count  = (BigInteger) this.getSession()
                            .createSQLQuery(sql)
                            .setParameter(0,email)
                            .uniqueResult();
        return count;
    }

    @Override
    public User findbyEmailAndPwd(String email, String password) {
        String sql="SELECT u.id,u.email,u.password,u.nickname,u.avatar,u.createTime,u.status,u.bindType,u.uniqueValue  FROM USER u WHERE u.email = ? AND u.password = ?";
        User user  = (User) this.getSession()
                .createSQLQuery(sql)
                .addEntity(User.class)
                .setParameter(0,email)
                .setParameter(1,password)
                .uniqueResult();
        return user;
    }
}
