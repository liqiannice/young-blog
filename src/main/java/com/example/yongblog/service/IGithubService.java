package com.example.yongblog.service;

import com.example.yongblog.bean.User;

import java.io.IOException;

/**
 * Created by LiQian_Nice on 2018/7/4
 */
public interface IGithubService {



    User login(String type, String code) throws IOException;
}
