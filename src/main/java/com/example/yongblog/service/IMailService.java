package com.example.yongblog.service;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

/**
 * Created by LiQian_Nice on 2018/7/6
 */
public interface IMailService {

    void sendEmail(String to) throws MessagingException, UnsupportedEncodingException;
}
