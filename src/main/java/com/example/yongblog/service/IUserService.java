package com.example.yongblog.service;

import com.example.yongblog.bean.User;

import java.math.BigInteger;

/**
 * Created by LiQian_Nice on 2018/7/4
 */

public interface IUserService {

    User findByUniqueValue(String uniqueValue);

    void save(User user);


    BigInteger findByEmail(String email);

    User findByEmailAndPwd(String email, String password);
}
