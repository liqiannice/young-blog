package com.example.yongblog.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.example.yongblog.dao.IUserDao;
import com.example.yongblog.service.IGithubService;
import com.example.yongblog.bean.User;
import com.example.yongblog.utils.TimeUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by LiQian_Nice on 2018/7/4
 */
@Service
public class GithubServiceImpl implements IGithubService {

    @Resource
    private IUserDao userDao;

    TimeUtil timeUtil=new TimeUtil();

    //github的clientid
    public static String GITHUB_CLIENT_ID="4863165f14c1470009ef";

    //github的clientsecret
    public static String GITHUB_CLIENT_SECRET="5d8b3e65401c6feeedeec7fd974391cc8bb33b71";

    @Override
    public User login(String type,String code) throws IOException {

        Map<String,String> heads = new HashMap<>();
        heads.put("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");

        Document doc = Jsoup.connect("https://github.com/login/oauth/access_token").ignoreContentType(true).headers(heads)
                .data("client_id",GITHUB_CLIENT_ID)
                .data("client_secret",GITHUB_CLIENT_SECRET)
                .data("code",code)
                .header("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36")
                .get();
        Element body = doc.body();
        String access_token = body.text().substring(body.text().indexOf("=")+1,body.text().indexOf("&"));
        Document userDetail = Jsoup.connect("https://api.github.com/user").ignoreContentType(true).headers(heads)
                .data("access_token",access_token)
                .header("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36")
                .get();
        JSONObject jsonObject=JSONObject.parseObject(userDetail.body().text());
        String uniqueValue=jsonObject.getString("node_id");
        User user=userDao.findByUniqueValue(uniqueValue);
        if (user!=null){
            return user;
        }else {
            user=new User();
            String nickName=jsonObject.getString("login");
            String  avatar_url=jsonObject.getString("avatar_url");
            String email=jsonObject.getString("email");
            user.setUniqueValue(uniqueValue);
            user.setAvatar(avatar_url);
            user.setBindType(type);
            user.setCreateTime(timeUtil.getCurrentDate());
            user.setEmail(email);
            user.setNickname(nickName);
            user.setStatus(true);
            userDao.save(user);
            return user;

        }
    }
}
