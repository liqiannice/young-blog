package com.example.yongblog.service.impl;

import com.example.yongblog.service.IMailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by LiQian_Nice on 2018/7/6
 */
@Service
@Slf4j
public class MailServiceImpl implements IMailService {

    @Resource
    private JavaMailSenderImpl mailSender;
    @Resource
    private VelocityEngine velocityEngine;

    @Override
    @Async
    public void sendEmail(String to) throws MessagingException, UnsupportedEncodingException {

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "GBK");
       // helper.setFrom("51103942@qq.com");//必须与配置文件发送邮件着一样，否则会报501认证失败错误
        helper.setFrom("51103942@qq.com","liqian");
        helper.setTo(to);
        helper.setSubject("测试HTM主题");
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("userName", "liqian");
        String mailText = VelocityEngineUtils.mergeTemplateIntoString(
                velocityEngine, "/vm/mail.vm", "UTF-8", model);
        System.out.println(mailText);
        helper.setText(mailText, true);
        mailSender.send(message);
    }
}
