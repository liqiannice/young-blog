package com.example.yongblog.service.impl;


import com.example.yongblog.bean.User;
import com.example.yongblog.dao.IUserDao;
import com.example.yongblog.service.IMailService;
import com.example.yongblog.service.IUserService;
import com.example.yongblog.utils.TimeUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

/**
 * Created by LiQian_Nice on 2018/7/4
 */
@Service
public class UserServiceImpl implements IUserService {

    @Resource
    private IUserDao userDao;

    @Resource
    private IMailService mailService;

    @Override
    public User findByUniqueValue(String uniqueValue) {
        return userDao.findByUniqueValue(uniqueValue);
    }

    @Override
    public void save(User user) {
        try {
            user.setAvatar("http://pc9wfx5gt.bkt.clouddn.com/avatar.jpg");//用户初始头像
            user.setCreateTime(TimeUtil.getCurrentDate());//注册时间
            user.setStatus(false);//未激活状态
            userDao.save(user);
            mailService.sendEmail(user.getEmail());
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }
    }



    @Override
    public User findByEmailAndPwd(String email, String password) {
        return userDao.findbyEmailAndPwd(email,password);
    }


    @Override
    public BigInteger findByEmail(String email) {
        return userDao.findByEmail(email);
    }
}
