package com.example.yongblog.utils;


import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;
import org.springframework.beans.factory.InitializingBean;


import java.io.File;
import java.io.InputStream;

/**
 * Created by LiQian_Nice on 2018/5/5
 */

public class QiNiuUtil implements  InitializingBean {

    private String accessKey="l9XiEBQZtiZ2P7qBtEnA28T4gVsK2iGzKstcBzO7";
    private String secretKey="Qa0F4d1zBr1we3jEhaC70lp6huWTNyZkA-edJsxf";


    private String bucket="learner";


    private String cdnPrefix="p89guys4i.bkt.clouddn.com";



    private StringMap putPolicy;


    //构造一个带指定Zone对象的配置类
    private Configuration cfg = new Configuration(Zone.autoZone());
    //...其他参数参考类注释
    private UploadManager uploadManager = new UploadManager(cfg);

    private Auth auth = Auth.create(accessKey, secretKey);

    public Response uploadFile(File file) throws QiniuException {
        Response response = this.uploadManager.put(file, null, getUploadToken());
        int retry = 0;
        while (response.needRetry() && retry < 3) {
            response = this.uploadManager.put(file, null, getUploadToken());
            retry++;
        }
        return response;
    }


    public Response uploadFile(InputStream inputStream) throws QiniuException {
        Response response = this.uploadManager.put(inputStream, null, getUploadToken(), null, null);
        int retry = 0;
        while (response.needRetry() && retry < 3) {
            response = this.uploadManager.put(inputStream, null, getUploadToken(), null, null);
            retry++;
        }
        return response;
    }




    public void afterPropertiesSet() throws Exception {
        this.putPolicy = new StringMap();
        putPolicy.put("returnBody", "{\"key\":\"$(key)\",\"hash\":\"$(etag)\",\"bucket\":\"$(bucket)\",\"width\":$(imageInfo.width), \"height\":${imageInfo.height}}");
    }


    /**
     * 获取上传凭证
     *
     * @return
     */
    private String getUploadToken() {
        return this.auth.uploadToken(bucket, null, 3600, putPolicy);
    }
}
