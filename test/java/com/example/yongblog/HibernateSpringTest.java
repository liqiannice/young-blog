package com.example.yongblog;

import com.example.yongblog.bean.User;
import com.example.yongblog.dao.IUserDao;
import com.example.yongblog.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;


import javax.annotation.Resource;
import java.math.BigInteger;

/**
 * Created by LiQian_Nice on 2018/7/22
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations ={ "classpath:spring/SpringApplicationContext.xml" })
@Transactional
@WebAppConfiguration
public class HibernateSpringTest {


    @Autowired
    IUserService userService; //用于接收一个UserServiceImpl实例



    @Test
    public void getCount()
    {
        BigInteger count = userService.findByEmail("@qq.com");
        log.info(String.valueOf(count));
    }
    @Test
    public void findByEmailAndPwd(){
        User user = userService.findByEmailAndPwd("51103942@qq.com","qian0711");
        log.info(user.getAvatar());
    }
}
